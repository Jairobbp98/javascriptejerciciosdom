const d = document, n = navigator;

const webCam = (id) => {
    const $video = d.getElementById(id);

    if (n.mediaDevices.getUserMedia) {
        n.mediaDevices.getUserMedia({
            video: true,
            audio: false
        })
        .then(strem => {
            console.log(strem);
            $video.srcObject = strem;
            $video.play()
        })
        .catch(err => {
            $video.insertAdjacentHTML("afterend", `<p><mark>Sucedio el siguiente error: ${err}</mark></p>`);
            console.log("Sucedio el siguiente error: " + err);
        });
    }
}

export { webCam }