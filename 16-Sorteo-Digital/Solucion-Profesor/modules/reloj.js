const d = document;

const digitalClock = (clock, btnPlay, btnStop) => {
  let clockTiempo;

  d.addEventListener("click", (e) => {
    if (e.target.matches(btnPlay)) {
      clockTiempo = setInterval(() => {
        let clockHour = new Date().toLocaleTimeString();
        d.querySelector(clock).innerHTML = `<h3>${clockHour}</h3>`;
      }, 1000);

      e.target.disabled = true;
    }

    if (e.target.matches(btnStop)) {
      clearInterval(clockTiempo);
      d.querySelector(clock).innerHTML = null;
    }
  });
};

const alarm = (sound, btnPlay, btnStop) => {
  let alarmaTiempo;
  const $alarma = d.createElement("audio");
  $alarma.src = sound;

  d.addEventListener("click", (e) => {
    if (e.target.matches(btnPlay)) {
      alarmaTiempo = setTimeout(() => {
        $alarma.play();
      }, 2000);

      e.target.disabled = true;
    }

    if (e.target.matches(btnStop)) {
        clearTimeout(alarmaTiempo);
        $alarma.pause();
        $alarma.currentTime = 0;
        d.querySelector(btnPlay).disabled = false;
    }
  });
};

export { digitalClock, alarm };