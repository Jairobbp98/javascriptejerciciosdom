const insertarSeccion = (audio) => {
  /* Creacion de los elementos */
  const $fragment = document.createDocumentFragment(),
        $templateForm = document.getElementById("seccionForm").content,
        $audio = document.querySelector(audio);

  /* Agregar atributos y contenido */
  $templateForm
    .querySelector("form-section")
    .children[0]
    .classList.add("Horas");
  $templateForm
    .querySelector("form-section")
    .children[2]
    .classList.add("Minutos");
  $templateForm
    .querySelector("form-section")
    .children[4]
    .classList.add("Segundos");

  /* Agregar contenido*/
  $templateForm.querySelector("form-section").children[0].textContent = "00";
  $templateForm.querySelector("form-section").children[2].textContent = "00";
  $templateForm.querySelector("form-section").children[4].textContent = "00";

  /* Crear Clone*/
  const $templateClone = document.importNode($templateForm, true);

  /* Crear fragmento e insertar */
  $fragment.appendChild($templateClone);
  $audio.after($fragment);
};

export { insertarSeccion };