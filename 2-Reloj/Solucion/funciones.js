import { iniciarAlarma, detenerAlarma } from "./components/alarma.js";
import { insertarSeccion } from "./components/fragmentos.js";
import { iniciarReloj, detenerReloj } from "./components/reloj.js";

const d = document;

d.addEventListener("DOMContentLoaded", () =>{
    insertarSeccion("audio");
    iniciarReloj(".IniciarReloj", ".Horas", ".Minutos", ".Segundos");
    detenerReloj(".DetenerReloj");
    iniciarAlarma(".IniciarAlarma", "audio");
    detenerAlarma(".DetenerAlarma", "audio");
});