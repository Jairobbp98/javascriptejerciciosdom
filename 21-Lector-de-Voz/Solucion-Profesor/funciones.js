import { slider } from "./modules/carrousel.js";
import { countdown } from "./modules/cuenta_regresiva.js";
import { networkStatus } from "./modules/deteccion_red.js";
import { searchFilters } from "./modules/filtro_busqueda.js";
import { getGeolocation } from "./modules/geolocalizacion.js";
import hamburguerMenu from "./modules/menuHamburguesa.js";
import { speechReader } from "./modules/narrador.js";
import { alarm, digitalClock } from "./modules/reloj.js";
import { scrollSpy } from "./modules/scroll_espia.js";
import { draw } from "./modules/sorteo.js";
import { contactFormValidation } from "./modules/validaciones_formulario.js";
import { smartVideo } from "./modules/video_inteligente.js";
import { webCam } from "./modules/webCam.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) =>{
    hamburguerMenu(".panel-btn",".panel", ".menu a");
    digitalClock("#reloj", "#activar-reloj", "#desactivar-reloj");
    alarm("music/alarma-good-4-morning.mp3", "#activar-alarma", "#desactivar-alarma");
    countdown("countdown", "May 23, 2021 03:23:19", "Feliz Cumpleaños Amigo 🎁");
    /* webCam("webcam"); */
    getGeolocation("geolocation");
    searchFilters(".card-filter", ".card");
    draw("#winner-btn", ".player");
    slider();
    scrollSpy();
    smartVideo();
    contactFormValidation();
});

speechReader();
/* networkStatus(); */