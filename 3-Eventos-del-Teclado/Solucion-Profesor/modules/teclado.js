const d = document;

const shortcuts = (e) => {
  console.log(e);
  console.log(e.type);
  console.log(e.key);
  console.log(e.keyCode);

  if (e.key === "a" && e.altKey)
    alert("Haz lanzado una alerta con el teclado");
  if (e.key === "q" && e.ctrlKey)
    confirm("Haz lanzado una confirmacion con el teclado");
  if (e.key === "e" && e.shiftKey)
    prompt("Haz lanzado una aviso con el teclado");
};

export { shortcuts };
