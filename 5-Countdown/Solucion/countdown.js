
const countdown = (Dias, Horas, Minutos, Segundos) => {
    setInterval(() => {
        const d = document,
            $dias = d.querySelector(Dias),
            $horas = d.querySelector(Horas),
            $minutos = d.querySelector(Minutos),
            $segundos = d.querySelector(Segundos);

        let dateInicio = new Date("12/17/2030 9:30 AM"),
            dateActual = new Date(),
            duracion = dateInicio - dateActual,
            miliSegundos = {
                segundos: 1000,
                minutos: function () {
                    return this.segundos * 60;
                },
                horas: function () {
                    return this.minutos() * 60;
                },
                dias: function () {
                    return this.horas() * 24;
                },
            };

        $dias.textContent = `${Math.floor(
            duracion / miliSegundos.dias()
        )}`;
        $horas.textContent = `${Math.floor(
            (duracion % miliSegundos.dias()) / miliSegundos.horas()
        )}`;
        $minutos.textContent = `${Math.floor(
            (duracion % miliSegundos.horas()) / miliSegundos.minutos()
        )}`;
        $segundos.textContent = `${Math.floor(
            (duracion % miliSegundos.minutos()) / miliSegundos.segundos
        )}`;

    }, 1000);
};

export { countdown };
