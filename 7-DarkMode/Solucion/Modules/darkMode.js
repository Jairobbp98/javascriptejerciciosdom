const d = document;

const darkModeBtn = (btn, darkClase) =>{
    const darkBoton = d.querySelector(btn);
    console.log(btn, darkClase);

    darkBoton.addEventListener("click", (e) => {
        if (e.target.matches(btn)) {
            
            d.body.classList.toggle(darkClase);

            if (d.body.classList.contains("dark")) {
                darkBoton.textContent = "🌞";
            }else {
                darkBoton.textContent = "🌚";
            }
        }
    })
}

export { darkModeBtn }