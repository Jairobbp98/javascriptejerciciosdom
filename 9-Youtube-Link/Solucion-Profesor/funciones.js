import { scrollTopBotton } from "./modules/botonScroll.js";
import { countdown } from "./modules/cuenta_regresiva.js";
import hamburguerMenu from "./modules/menuHamburguesa.js";
import { responsiveMedia } from "./modules/objeto_resposive.js";
import { alarm, digitalClock } from "./modules/reloj.js";
import { darkTheme } from "./modules/tema_Oscuro.js";

const d = document;

d.addEventListener("DOMContentLoaded", (e) => {
  hamburguerMenu(".panel-btn", ".panel", ".menu a");
  digitalClock("#reloj", "#activar-reloj", "#desactivar-reloj");
  alarm(
    "music/alarma-good-4-morning.mp3",
    "#activar-alarma",
    "#desactivar-alarma"
  );
  countdown("countdown", "May 23, 2021 03:23:19", "Feliz Cumpleaños Amigo 🎁");
  scrollTopBotton(".scroll-top-btn");
  responsiveMedia(
    "youtube",
    "(min-width: 1024px)",
    `<a href="https://www.youtube.com/embed/8Sokxte6rfY" target="_blank" rel="noopener">ver video</a>`,
    `<iframe width="560" height="315" src="https://www.youtube.com/embed/8Sokxte6rfY" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>`
  );
  responsiveMedia(
    "gmaps",
    "(min-width: 1024px)",
    `<a href="https://goo.gl/maps/3w8GDP7pykzTRK9o9" target="_blank" rel="noopener">ver mapa</a>`,
    `<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d15601.789708052595!2d-86.28694965!3d12.1499123!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f71567aa64589a3%3A0x4d769c7f5f80482d!2sHospital%20Salud%20Integral%2C%20Managua!5e0!3m2!1ses!2sni!4v1603045588119!5m2!1ses!2sni" width="600" height="450" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>`
  );
});

darkTheme(".dark-theme-btn", "dark-mode");
